const Lab = require('lab');
const lab = exports.lab = Lab.script();
const Code = require('code');
const sinon = require('sinon');
const uuid = require('node-uuid');

const mongoose = require('mongoose');
const mockgoose = require('mockgoose');

lab.experiment('# Book resource', () => {
  let db = {
    books: require('../../../app/models/book')
  };
  let BookResources;

  lab.before((done) => {
    mockgoose(mongoose).then(() => {
      mongoose.connect('mongodb://example.com/TestingDB', (err) => {
        BookResources = require('../../../app/resources/books')(db);
        done(err);
      });
    });
  });

  lab.test('POST /books', (done) => {
    const book = {
      title: 'Livro 1 ---',
      author: 'Autor 1 ----'
    };

    const request = {
      payload: book
    };

    const bookDoc = book;
    bookDoc._id = uuid.v1();

    const stub = sinon.stub(db.books, 'create');
    db.books.create.callsArgWith(1, null, bookDoc);

    BookResources.create(request, (res) => {
      Code.expect(res._id).to.equal(bookDoc._id);
      stub.restore();
      done();
    });
  });

  lab.test('POST /books (error)', (done) => {
    const book = {
      title: 'Livr'
    };

    const request = {
      payload: book
    };

    const reply = sinon.spy();
    const stub = sinon.stub(db.books, 'create');
    db.books.create.callsArgWith(1, null, new Error('error'));

    BookResources.create(request, reply);

    sinon.assert.calledOnce(reply);
    sinon.assert.calledWith(reply, sinon.match.instanceOf(Error));
    stub.restore();
    done();
  });

  lab.test.only('GET /books', (done) => {
    const reply = sinon.spy();
    const stub = sinon.stub(db.books, 'find').yields(null, []);

    BookResources.list({}, reply);

    sinon.assert.calledOnce(reply);
    sinon.assert.calledWith(reply);

    stub.restore();
    done();
  });
});
