FROM node:6.5.0
EXPOSE 3000
RUN mkdir /web
WORKDIR /web
RUN cd /web

CMD ["node", "app/server.js"]
