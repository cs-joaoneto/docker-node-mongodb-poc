const Boom = require('boom');
const uuid = require('node-uuid');

module.exports = function BookResource(db) {
  return {
    list: function (request, reply) {
      db.books.find((err, docs) => {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        reply(docs);
      });
    },

    find: function (request, reply) {
      db.books.findOne({
        _id: request.params.id
      }, (err, doc) => {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        if (!doc) {
          return reply(Boom.notFound());
        }

        reply(doc);
      });
    },

    create: function (request, reply) {
      const book = request.payload;
      //Create an id
      book._id = uuid.v1();

      db.books.create(book, (err, book) => {
        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        reply(book);
      });
    },

    update: function (request, reply) {
      db.books.update({
        _id: request.params.id
      }, {
        $set: request.payload
      }, function (err, result) {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        if (result.n === 0) {
          return reply(Boom.notFound());
        }

        reply().code(204);
      });
    },
    delete: function (request, reply) {
      db.books.remove({
        _id: request.params.id
      }, function (err, result) {

        if (err) {
          return reply(Boom.wrap(err, 'Internal MongoDB error'));
        }

        if (result.n === 0) {
          return reply(Boom.notFound());
        }

        reply().code(204);
      });
    }
  };
};
