const Hapi = require('hapi');
const server = new Hapi.Server();
const mongoose = require('mongoose');

// Connection
mongoose.connect('mongodb://mongo:27017/docker-node-mongodb-poc');

// Models
server.app.db = {
  books: require('./models/book')
};

server.connection({ port: 3000 });

// Routes
server.register([
  require('./routes/books')
], (err) => {
  if (err) {
    throw err;
  }

  // Start the server
  server.start(() => {
    console.log('Server running at:', server.info.uri);
  });
});

module.exports = server;
