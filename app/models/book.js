const Joi = require('joi');
const uuid = require('node-uuid');
const mongoose = require('mongoose');

const Book = new mongoose.Schema({
  _id: {
    type: String,
    default: () => uuid.v1()
  },
  title: String,
  author: String,
  isbn: Number
});

Book.statics.validations = {
  _id: Joi.string(),
  title: Joi.string().min(10).max(50).required(),
  author: Joi.string().min(10).max(50).required(),
  isbn: Joi.number()
};

module.exports = mongoose.model('Book', Book);
