const uuid = require('node-uuid');
const Joi = require('joi');

function Model(data, options) {
  if (!options || options._id !== false) {
    this._id = data._id || uuid.v1();
  }

  for(var prop in data) {
    this[prop] = data[prop];
  }
}

Model.prototype.getProps = function () {
  return Object.keys(this);
};

Model.prototype.validate = function (callback) {
  Joi.validate(this, this.constructor.schema, callback);
};

module.exports = Model;
