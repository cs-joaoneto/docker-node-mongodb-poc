const Joi = require('joi');

exports.register = function (server, options, next) {
  const db = server.app.db;
  const BookResource = require('../resources/books')(db);

  server.route({
    method: 'GET',
    path: '/books',
    handler: BookResource.list
  });

  server.route({
    method: 'GET',
    path: '/books/{id}',
    handler: BookResource.find
  });

  server.route({
    method: 'POST',
    path: '/books',
    handler: BookResource.create,
    config: {
      validate: {
        payload: db.books.validations
      }
    }
  });

  server.route({
    method: 'PATCH',
    path: '/books/{id}',
    handler: BookResource.update,
    config: {
      validate: {
        payload: Joi.object(db.books.validations).required().min(1)
      }
    }
  });

  server.route({
    method: 'DELETE',
    path: '/books/{id}',
    handler: BookResource.delete
  });

  return next();
};

exports.register.attributes = {
  name: 'routes-books'
};
